package be.kriscoolen.arrayManager;

import java.util.Random;

public class StringArrayManager {
    private String[] list;
    private int max_size; //size of the array list
    private int size; //number of strings currently in the list
    Random rand = new Random();

    public StringArrayManager(int max_size){
        if(max_size >0) {
            list = new String[max_size];
            size = 0;
            this.max_size = max_size;
        }
        else{
            list = null;
            size = 0;
            max_size = 0;
            System.out.println("The size of the array should be greater than zero.");
        }
    }


    public String getFirstValue(){
        if(size ==0) return null;
        else return list[0];
    }

    public String getLastValue(){
        if(size ==0) return null;
        else return list[size -1];
    }

    public String getRandomValue(){
        if(size ==0) return null;
        else return list[rand.nextInt(size)];
    }

    public String getHighestValue(){
        if(size ==0) return null;
        else{
            String max = list[0];
            for(int i = 1; i< size; i++){
                if(list[i].compareToIgnoreCase(max)>0) max = list[i];
            }
            return max;
        }
    }

    public String getLowestValue(){
        if(size ==0) return null;
        else{
            String min = list[0];
            for(int i = 1; i< size; i++){
                if(list[i].compareToIgnoreCase(min)<0) min = list[i];
            }
            return min;
        }
    }

    public int getSize(){
        return size;
    }

    public String[] getList() {
        return list;
    }

    public int getMax_size() {
        return max_size;
    }

    public boolean removeValue(String value){
        if(value==null) return false;
        int index = -1;
        for(int i = 0; i< size; i++){
            if(value.compareTo(list[i])==0){
                index = i;
            }
        }
        if(index==-1){
            return false;
        }
        else{ //remove value at position 'index'
            if(index< size -1) for(int i = index+1; i< size; i++) list[i-1]=list[i];
            list[size -1]=null;
            size--;
            return true;
            }
    }

    public boolean append(String value){
        if(value==null) return false;
        //if element is already in list we exit the function and no element is appended.
        for(int i = 0; i< size; i++){
            if(value.compareTo(list[i])==0){
                return false;
            }
        }
        if(size < max_size){
            list[size] = value;
            size++;
            return true;
        }
        else{
            return false;
        }
    }

    public String getValue(int index){
        if(index<0 || index>= size) return null;
        else{
            return list[index];
        }
    }

    @Override
    public String toString() {
        String stringList="";
        if(size >0) {
            for (int i = 0; i < size - 1; i++) {
                stringList += list[i] + ", ";
            }
            stringList += list[size - 1];
        }
        return "[list={" + stringList +
                "}, size=" + max_size +
                ", nElements=" + size +
                ']';
    }
}
