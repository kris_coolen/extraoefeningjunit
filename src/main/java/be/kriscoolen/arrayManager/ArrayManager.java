package be.kriscoolen.arrayManager;

import java.lang.reflect.Array;
import java.util.Random;

public class ArrayManager<myType extends Comparable<myType>> {
    private Object[] list;
    private int max_size; //size of the array list
    private int size; //number of strings currently in the list
    Random rand = new Random();

    public ArrayManager(int max_size){
        if(max_size >0) {
            list = new Object[max_size];
            size = 0;
            this.max_size = max_size;
        }
        else{
            list = null;
            size = 0;
            max_size = 0;
            System.out.println("The size of the array should be greater than zero.");
        }
    }


    public myType getFirstValue(){
        if(size ==0) return null;
        else return (myType) list[0];
    }

    public myType getLastValue(){
        if(size ==0) return null;
        else return (myType) list[size -1];
    }

    public myType getRandomValue(){
        if(size ==0) return null;
        else return (myType) list[rand.nextInt(size)];
    }

    public myType getHighestValue(){
        if(size ==0) return null;
        else{
            myType max = (myType) list[0];

            for(int i = 1; i< size; i++){
                if(max.compareTo((myType)list[i])>0) max = (myType) list[i];
            }
            return max;
        }
    }

    public myType getLowestValue(){
        if(size ==0) return null;
        else{
            myType min = (myType) list[0];
            for(int i = 1; i< size; i++){
                if(min.compareTo((myType) list[i])<0) min = (myType) list[i];
            }
            return min;
        }
    }

    public int getSize(){
        return size;
    }

    public myType[] getList() {
        return (myType[]) list;
    }

    public int getMax_size() {
        return max_size;
    }

    public boolean removeValue(myType value){
        if(value==null) return false;
        int index = -1;
        for(int i = 0; i< size; i++){
            if(value.compareTo((myType) list[i])==0){
                index = i;
            }
        }
        if(index==-1){
            return false;
        }
        else{ //remove value at position 'index'
            if(index< size -1) for(int i = index+1; i< size; i++) list[i-1]=list[i];
            list[size -1]=null;
            size--;
            return true;
        }
    }

    public boolean append(myType value){
        if(value==null) return false;
        //if element is already in list we exit the function and no element is appended.
        for(int i = 0; i< size; i++){
            if(value.compareTo((myType) list[i])==0){
                return false;
            }
        }
        if(size < max_size){
            list[size] = value;
            size++;
            return true;
        }
        else{
            return false;
        }
    }

    public myType getValue(int index){
        if(index<0 || index>= size) return null;
        else{
            return (myType) list[index];
        }
    }

}
