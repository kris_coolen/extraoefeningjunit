package be.kriscoolen.boekenWorm;

import be.kriscoolen.arrayManager.ArrayManager;
import be.kriscoolen.arrayManager.StringArrayManager;
public class BoekenWormApp {

   // StringArrayManager sam = new StringArrayManager(100);
    ArrayManager<String> sam = new ArrayManager<String>(100);

    public static void main(String[] args) {
        BoekenWormApp app = new BoekenWormApp();
        app.startApp();
    }


    public void ingevenBoeken(){
        if(sam.getSize()==sam.getMax_size()) {
            ConsoleInputTool.printAlert("Boekenlijst is vol. Je kan geen nieuwe boeken ingeven.");
            return;
        }
        System.out.println("Invoeren boeken:");
        System.out.println("Typ woordje 'stop' om naar het menu te gaan");
        String answer = "";
        while(true) {
            answer = ConsoleInputTool.askUserInputString("Geef titel: ", 1);
            if(answer.compareTo("stop")==0) return;
            if(!sam.append(answer))  ConsoleInputTool.printAlert("Titel " + answer + " is al in lijst aanwezig!");
            else System.out.println("Boek ingeschreven!");
            if(sam.getSize()==sam.getMax_size()){
                ConsoleInputTool.printAlert("Boekenlijst is nu vol. Je keert terug naar de menu.");
                return;
            }
        }
    }

    public void kiezenBoeken(){
        int choice=-1;
        while(true){
            showMenuBoekKeuze();
            choice=ConsoleInputTool.askUserInputInteger("Uw keuze: ",0,5);
            String book = "";
            switch(choice){
                case 0: return;
                case 1:
                    //eerste boek selecteren
                    book = sam.getFirstValue();
                    break;
                case 2:
                    //laatste uit de lijst selecteren
                    book = sam.getLastValue();
                    break;
                case 3:
                    //willekeurig boek uit de lijst selecteren
                    book = sam.getRandomValue();
                    break;
                case 4:
                    //eerste alfabetische boek uit de lijst selecteren
                    book = sam.getLowestValue();
                    break;

                case 5:
                    //laatste alfabetische boek uit de lijst selecteren
                    book = sam.getHighestValue();
                    break;
            }
            if(!removeBook(book))
                ConsoleInputTool.printAlert("Boek kon niet verwijderd worden.");
        }
    }

    public boolean removeBook(String book){
        if(book!=null){
            System.out.println("Boek: "+book);
            if(ConsoleInputTool.askUserYesNoQuestion("Wenst u dit boek te kiezen?(y/n)",false,false)){
                if(sam.removeValue(book)){
                    System.out.println("Boek \""+book+"\" werd verwijderd uit de lijst.");
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public void overzichtBoeken(){
        int choice = -1;
        while(choice!=0){
            showMenuOverzicht();
            choice = ConsoleInputTool.askUserInputInteger("Uw keuze: ", 0,2);
            switch(choice){
                case 1: printList(); break;
                case 2: printListAlphabetically(); break;
            }
        }
    }

    void printList(){
        for(int i=0; i<sam.getSize(); i++){
            System.out.printf("%d. %s\n",i,sam.getValue(i));
        }

    }

   /*void printListAlfa(){
        String[] sortedBookList = Arrays.copyOf(sam.getList(),sam.getSize());
        Arrays.sort(sortedBookList);
        for(int i=0; i<sortedBookList.length; i++){
            System.out.printf("%d. %s\n",i,sortedBookList[i]);
        }
    }*/

  void printListAlphabetically(){
        StringArrayManager copy = new StringArrayManager(sam.getMax_size());
        for(int i=0; i<sam.getSize();i++) copy.append(sam.getValue(i));
        String[] sortedList = new String[sam.getSize()];
        for(int i=0; i<sortedList.length;i++){
            sortedList[i]=copy.getLowestValue();
            copy.removeValue(copy.getLowestValue());
        }
        for(int i=0; i<sortedList.length;i++){
            System.out.printf("%d. %s\n",i,sortedList[i]);
        }
  }



    public void startApp(){
        ingevenBoeken();
        int choice=-1;
        while(choice!=0){
            showMenuBoekenWorm();
            choice = ConsoleInputTool.askUserInputInteger("Uw keuze: ",0,3);
            switch(choice){
                case 1: ingevenBoeken();break;
                case 2: kiezenBoeken(); break;
                case 3: overzichtBoeken(); break;
            }
        }
    }

    public static void showMenuBoekenWorm(){
        System.out.println("BoekenWorm menu");
        System.out.println("1) ingeven boeken");
        System.out.println("2) kiezen boeken");
        System.out.println("3) overzicht boeken");
        System.out.println("0) stoppen");
    }

    public static void showMenuBoekKeuze(){
        System.out.println("Boek keuze menu");
        System.out.println("1) Eerste uit de lijst");
        System.out.println("2) Laatste uit de lijst");
        System.out.println("3) Willekeurig boek");
        System.out.println("4) Eerste alfabetisch boek");
        System.out.println("5) Laatste alfabetische boek");
        System.out.println("0) Terug naar hoofdmenu");
    }

    public static void showMenuOverzicht(){
        System.out.println("Overzicht menu");
        System.out.println("1) Volgorde lijst");
        System.out.println("2) Volgorde alfabetisch");
        System.out.println("0) Terug naar hoofdmenu");
    }
}
