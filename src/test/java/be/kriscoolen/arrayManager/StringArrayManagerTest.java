package be.kriscoolen.arrayManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StringArrayManagerTest {

    StringArrayManager arrayManager;

    /**
     * Test function to test the constructor ArrayManager(int max_size)
     */
    @Test
    public void ArrayManagerTest(){
        arrayManager = new StringArrayManager(10);
        assertEquals(10,arrayManager.getMax_size());
        assertEquals(0,arrayManager.getSize());
        arrayManager = new StringArrayManager(0);
        assertNull(arrayManager.getList());
        assertEquals(0,arrayManager.getSize());
        assertEquals(0,arrayManager.getMax_size());
        arrayManager = new StringArrayManager(-5);
        assertNull(arrayManager.getList());
        assertEquals(0,arrayManager.getSize());
        assertEquals(0,arrayManager.getMax_size());
    }
    /**
     * Test function to test the function getFirstValue() and getLastValue();
     */
    @Test
    public void getFirstAndLastValueTest(){
        arrayManager = new StringArrayManager(100);
        assertNull(arrayManager.getFirstValue());
        assertNull(arrayManager.getLastValue());
        arrayManager.append("book1");
        arrayManager.append("book2");
        arrayManager.append("book3");
        assertEquals(0,arrayManager.getFirstValue().compareTo("book1"));
        assertEquals(0,arrayManager.getLastValue().compareTo("book3"));
    }

    /**
     * Test function to test the function getRandomValue();
     */
    @Test
    public void getRandomValueTest(){
        arrayManager = new StringArrayManager(100);
        assertNull(arrayManager.getRandomValue());
        arrayManager.append("book1");
        arrayManager.append("book2");
        arrayManager.append("book3");
        for(int nExperiments=0; nExperiments<10; nExperiments++) {
            assertNotNull(arrayManager.getRandomValue());
        }

    }

    /**
     * Test function to test the function getHighestValue() or getLowestValue();
     */
    @Test
    public void getHighestOrLowestValueTest(){
        arrayManager = new StringArrayManager(100);
        assertNull(arrayManager.getLowestValue());
        assertNull(arrayManager.getHighestValue());
        arrayManager.append("b book");
        arrayManager.append("a book");
        arrayManager.append("d book");
        arrayManager.append("c book");
        assertEquals(0,arrayManager.getLowestValue().compareTo("a book"));
        assertEquals(0,arrayManager.getHighestValue().compareTo("d book"));

    }

    /**
     * Test function to test the function getSize() and getMaxsize();
     */
    @Test
    public void getSizeOrMax_sizeTest(){
        arrayManager = new StringArrayManager(100);
        assertEquals(0,arrayManager.getSize());
        assertEquals(100, arrayManager.getMax_size());
        arrayManager.append("book1");
        arrayManager.append("book2");
        arrayManager.append("book3");
        arrayManager.append("book4");
        assertEquals(4,arrayManager.getSize());

    }

    /**
     * Test function to test the functions append(), remove() and getValue;
     */
    @Test
    public void appendAndRemoveTest(){
        arrayManager = new StringArrayManager(100);
        assertFalse(arrayManager.append(null));
        assertEquals(0,arrayManager.getSize());
        assertTrue(arrayManager.append("book1"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(1,arrayManager.getSize());
        assertTrue(arrayManager.append("book2"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(0,arrayManager.getValue(1).compareTo("book2"));
        assertEquals(2,arrayManager.getSize());
        assertTrue(arrayManager.append("book3"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(0,arrayManager.getValue(1).compareTo("book2"));
        assertEquals(0,arrayManager.getValue(2).compareTo("book3"));
        assertEquals(3,arrayManager.getSize());
        assertFalse(arrayManager.append("book3"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(0,arrayManager.getValue(1).compareTo("book2"));
        assertEquals(0,arrayManager.getValue(2).compareTo("book3"));
        assertEquals(3,arrayManager.getSize());
        assertFalse(arrayManager.removeValue("book4"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(0,arrayManager.getValue(1).compareTo("book2"));
        assertEquals(0,arrayManager.getValue(2).compareTo("book3"));
        assertEquals(3,arrayManager.getSize());
        assertTrue(arrayManager.removeValue("book2"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book1"));
        assertEquals(0,arrayManager.getValue(1).compareTo("book3"));
        assertNull(arrayManager.getValue(2));
        assertEquals(2,arrayManager.getSize());
        assertTrue(arrayManager.removeValue("book1"));
        assertEquals(0,arrayManager.getValue(0).compareTo("book3"));
        assertNull(arrayManager.getValue(1));
        assertNull(arrayManager.getValue(2));
        assertEquals(1,arrayManager.getSize());
        assertTrue(arrayManager.removeValue("book3"));
        assertNull(arrayManager.getValue(0));
        assertNull(arrayManager.getValue(1));
        assertNull(arrayManager.getValue(2));
        assertEquals(0,arrayManager.getSize());
    }






}

